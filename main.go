package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

type Result struct {
	Date  string  `json:"date"`
	Value float32 `json:"value"`
}

func initDb() (db *sql.DB) {
	db, err := sql.Open("mysql", "root:mypassword@tcp(0.0.0.0:6604)/journal")
	if err != nil {
		fmt.Printf("got err, %+v\n", err)
	}

	fmt.Print("Hello from DB")

	err = db.Ping()
	if err != nil {
		panic(err)
	}
	return db
}

func hello(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "hello\n")
}

func getTemp(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	getDB := initDb()

	rows, err := getDB.Query(`SELECT date, value FROM journal`)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var results []Result

	for rows.Next() {
		var result Result
		err = rows.Scan(&result.Date, &result.Value)
		if err != nil {
			panic(err)
		}
		results = append(results, result)
		fmt.Println(result)
	}
	json.NewEncoder(w).Encode(results)
}

func postTemp(w http.ResponseWriter, r *http.Request) {
	postDB := initDb()

	stmt, err := postDB.Prepare("INSERT INTO journal(date, value) VALUES(now(), ?)")
	if err != nil {
		panic(err.Error())
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err.Error())
	}

	keyVal := make(map[string]string)
	json.Unmarshal(body, &keyVal)
	value := keyVal["value"]

	_, err = stmt.Exec(value)
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintf(w, "New temp was added")
}

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", hello)
	router.HandleFunc("/get", getTemp).Methods("GET")
	router.HandleFunc("/post", postTemp).Methods("POST")

	log.Fatal(http.ListenAndServe("192.168.1.104:8000", router))
}
